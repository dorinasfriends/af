import io
import random
from flask import Response
from flask import Flask
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

app = Flask(__name__)

@app.route('/plot.png')
def plot_png():
    test_df = pd.read_csv('ecg_data/normal1.csv', header=None)
    X_test = test_df.iloc[:, :187].values[0:9].flatten()
    X_test = X_test[X_test > 0]
    X_test = X_test[X_test < 1]
    fig = create_figure(X_test)
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def create_figure(input):
    fig = Figure( figsize=(20, 5), dpi=80, facecolor='w', edgecolor='k')
    axis = fig.add_subplot(1, 1, 1)
    axis.plot(input)
    return fig




if __name__ == '__main__':
    app.run()