from keras.datasets import cifar10
from keras.utils import to_categorical
from keras.models import load_model
import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder

def two_labels(x):
    if x>0:
        return 1
    return 0

def predict(input):

    model = load_model("my_model.h5")
    scores = model.predict(input)
    label_result =np.argmax(scores)
    print(label_result)
    return label_result

if __name__ == '__main__':
    # Load the dataset
    ##not necessary
    test_df=pd.read_csv('ecg_data/mitbih_test.csv',header=None)
    target_test = test_df[187]
    target_test = np.vectorize(two_labels)(target_test)
    print(target_test)
    y_test = to_categorical(target_test)
    X_test = test_df.iloc[:, :187].values[0]
    X_test = X_test[np.newaxis,:,np.newaxis]
    ### not necessary

    predict(X_test)

